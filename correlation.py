import numpy as np
import matplotlib.pyplot as plt

amp_f = 5.
xf=5.
amp_g = -1.
xg=-5.

npts = 200.
width = 40.
x = np.arange(npts) / (npts-1.) * width - width / 2.

f = amp_f * np.sin(x-xf) / (x - xf)
g = amp_g * np.sin(x-xg) / (x - xg)


#mean values:
fmean = 1. / width * np.trapz(f,x)
gmean = 1. / width * np.trapz(g,x)
print(fmean,gmean)
fig,(ax1,ax2) = plt.subplots(2,1,sharex=True)
ax1.plot(x,f-fmean)
ax1.plot(x,g-gmean)
ax1.axhline(c='grey')
ax1.axvline(c='grey')

#standard deviations:
fstd = np.sqrt(1. / width * np.trapz((f - fmean)**2,x))
gstd = np.sqrt(1. / width * np.trapz((g - gmean)**2,x))
print(fstd,gstd)
#cheat with python:
print(np.std(f),np.std(g))

#covariance and correlation coefficients:
cov_f_g = 1. / width * np.trapz((f - fmean) * (g - gmean),x)
cor_f_g = cov_f_g / (fstd * gstd)
print(cov_f_g,cor_f_g)

cor_func_f_g = []
for x0 in x:
    xtmp = x - x0
    ftmp = amp_f * np.sin(xtmp-xf) / (xtmp - xf)
    wh=np.isfinite(ftmp)
    cor_func_f_g.append(1. / width * np.trapz(ftmp[wh] * (g[wh] - gmean),x[wh])
                        / (fstd * gstd))

ax2.plot(x,np.asarray(cor_func_f_g))
ax2.axhline(c='grey')
ax2.axvline(c='grey')

plt.show()
