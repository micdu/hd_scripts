import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

nt = 300
theta = np.arange(nt)/(nt-1.) * 2.*np.pi

r0 = 1.0
x0=0.
y0=0.

x = r0 * np.cos(theta) + x0
y = r0 * np.sin(theta) + y0

fig,ax=plt.subplots()
plt.subplots_adjust(left=0.25,bottom=0.25)

u = (x + x / (x**2 + y**2))
v = (y - y / (x**2 + y**2))

ax.plot(x,y,label='circle')
ax.plot(u,v,lw=2,label='Joukowski transform')
ax.axis('equal')
ax.legend()

axr = plt.axes([0.25, 0.15, 0.65, 0.03])
axx0 = plt.axes([0.25, 0.10, 0.65, 0.03])
axy0 = plt.axes([0.25, 0.05, 0.65, 0.03])
axconst = plt.axes([0.25, 0.2, 0.65, 0.03])
#axconst = plt.axes([0.25,0.2,0.65,0.03])

srad = Slider(axr,'radius',0.5,2.0,valinit=r0)
sx0 =  Slider(axx0,'x0',-5.0,5.0,valinit=x0)
sy0 =  Slider(axy0,'y0',-5.0,5.0,valinit=y0)
sconst = Slider(axconst,'gamma const.',-5.,5.,valinit=0)

#sconst = Slider(axconst,'gamma const.',-5.,5.,valinit=0)

def update(val):
    r=srad.val
    xmid=sx0.val
    ymid=sy0.val
    const = sconst.val

    
    x = r * np.cos(theta) + xmid
    y = r * np.sin(theta) + ymid

    u = (x + x / (x**2 + y**2))
    v = (y - y / (x**2 + y**2))
    ax.clear()
    ax.plot(x,y,label='circle')
    ax.plot(u,v,lw=2,label='Joukowski transform')
    ax.legend()
    
    X=np.arange(-3,3, 0.1)
    Y=np.arange(-3,3, 0.1)
    xgrid,ygrid=np.meshgrid(X,Y)
    wh = np.where(xgrid**2 + ygrid**2 < r)
    xgrid[wh] = r
    ygrid[wh] = r
    zgrid = xgrid + ygrid * 1j
    
    alpha = r
    beta = xmid + 1j*ymid
    v_inf = 10.
    R0 = 1.5
    gamma = const * 2. * np.pi * v_inf * R0
    
    
    xsci = zgrid + 0.8**2 / zgrid
    U = np.zeros(zgrid.shape,dtype=complex)
    for m in range(zgrid.shape[0]):
    	for n in range(zgrid.shape[1]):
    		U[m,n] = gamma * np.log(zgrid[m,n]/R0) / (2. * np.pi)
    psi = (v_inf * zgrid + (v_inf  * R0**2) / zgrid - 1j*U).imag
    #print(np.min(psi),np.max(psi))
    #print(zgrid[10,10],psi[35,35],psi[10,10])
    psi_surf = -gamma / 2./np.pi * np.log(R0)

    #levels = np.arange(100)/99. * 20. - 13. 
    levels=np.sort(np.append((np.arange(100)/99.*2.-1.)*40.,psi_surf))
    
    ugrid = xgrid + xmid
    vgrid = ygrid + ymid

    ugrid = (ugrid + ugrid / (ugrid**2 + vgrid**2))
    vgrid = (vgrid - vgrid / (ugrid**2 + vgrid**2))

    ax.contour(ugrid,vgrid,psi,levels,linestyles='solid')
    ax.set_xlim((np.min(u*1.3),np.max(u*1.3)))
    ax.set_ylim((np.min(v*1.3),np.max(v*1.3)))

    fig.canvas.draw_idle()

srad.on_changed(update)
sx0.on_changed(update)
sy0.on_changed(update)
sconst.on_changed(update)

plt.show()
