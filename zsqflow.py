import numpy as np
import matplotlib.pyplot as plt

nx = 100
x = np.tile((np.arange(nx)/(nx-1.) * 2. - 1.) * 2.,(nx,1))
y=np.transpose(x)
#print x.shape
#print y.shape

'''
for f(z) = z^2
have z = a + ib
f(z) = a^2 - b^2 + i2ab
'''

phi = x**2 - y**2
psi = 2 * x * y

fig = plt.figure()
st = fig.suptitle("f(z) = z^2", fontsize="x-large")

ax1 = fig.add_subplot(131)
ax1.contour(x,y,phi,colors='blue',linestyles='solid')
ax1.set_title('Velocity potential (phi)')

ax2 = fig.add_subplot(132)
ax2.contour(x,y,psi,colors='red',linestyles='solid')
ax2.set_title('Streamlines (psi)')

ax3 = fig.add_subplot(133)
ax3.contour(x,y,phi,colors='blue',linestyles='solid')
ax3.contour(x,y,psi,colors='red',linestyles='solid')
ax3.set_title('Both')

plt.show()


