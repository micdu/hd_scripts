import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

nx = 100
x = np.tile((np.arange(nx)/(nx-1.) * 2. - 1.) * 5.,(nx,1))
y=np.transpose(x)

'''
w(z) = v_inf * z + gamma / (i* 2pi) * ln(z) + v_inf *R^2/z
R = circle radius
gamma = circulation

'''

twopi = 2.*np.pi

R_0 = 1.5
v_inf0=10.
const0=0.
gamma = const0 * 2.*twopi * v_inf0*R_0


psi = -gamma / (2. * twopi) * np.log(x**2 + y**2) \
    + v_inf0 * y * (1. - R_0**2 / (x**2 + y**2))

#plt.plot(psi)
#plt.show()
levels=np.sort(np.append((np.arange(50)/49./2.-1.)*20.,0.))

circle1 = plt.Circle((0,0),R_0,color='gray')

fig,ax=plt.subplots()
plt.subplots_adjust(left=0.25, bottom=0.25)
ax.contour(x,y,psi,levels,linestyles='solid')
ax.add_artist(circle1)


axconst = plt.axes([0.25, 0.1, 0.65, 0.03])
sconst = Slider(axconst,'gamma const.',-5.,5.,valinit=const0)

def update(val):
    const = sconst.val
    gamma = const * 2.*twopi * v_inf0*R_0
    psi = -gamma / (2. * twopi) * np.log(x**2 + y**2) \
    + v_inf0 * y * (1. - R_0**2 / (x**2 + y**2))

    psi_surf = -gamma / twopi * np.log(R_0)
    levels=np.sort(np.append((np.arange(100)/99.*2.-1.)*40.,psi_surf))

    ax.clear()
    ax.contour(x,y,psi,levels,linestyles='solid')
    ax.add_artist(circle1)

    fig.canvas.draw_idle()

sconst.on_changed(update)

plt.show()
plt.show()
