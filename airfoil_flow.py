import numpy as np
import numpy.ma as ma
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

def J(z, lam, alpha):
    return z+(np.exp(-1j*2*alpha)*lam**2)/z

def circle(C, R):
    t = np.linspace(0,2*np.pi, 200)
    return C + R*np.exp(1j*t)

def deg2radians(deg):
    return deg*np.pi/180

def flowlines(alpha=10, beta=5, V_inf=1, R=1, ratio=1.2,const=4.):
    #alpha, beta are given in degrees
    #ratio =R/lam
    alpha = deg2radians(alpha)# angle of attack
    beta = deg2radians(beta)# -beta is the argument of the complex no (Joukowski parameter - circle center)
    if ratio <= 1: #R/lam must be >1
        raise ValueError('R/lambda must be > 1')
    lam = R/ratio#lam is the parameter of the Joukowski transformation
   
    center_c = np.exp(-1j*alpha)*(lam-R*np.exp(-1j*beta))

    Circle = circle(center_c,R)
    Airfoil = J(Circle, lam, alpha)
    X = np.arange(-3, 3, 0.1)
    Y = np.arange(-3, 3, 0.1)

    x,y = np.meshgrid(X, Y)
    z = x+1j*y
    z = ma.masked_where(np.absolute(z-center_c)<=R, z)
    w = J(z, lam, alpha)
    beta = beta+alpha
    Z = z-center_c

    Gamma = -const*np.pi*V_inf*R*np.sin(beta)#circulation
    U = np.zeros(Z.shape, dtype=complex)
    with np.errstate(divide='ignore'):#
        for m in range(Z.shape[0]):
            for n in range(Z.shape[1]):# due to this numpy bug https://github.com/numpy/numpy/issues/8516
                                       #we evaluate  this term of the flow elementwise
                U[m,n] = Gamma*np.log((Z[m,n])/R)/(2*np.pi)
    c_flow = V_inf*Z + (V_inf*R**2)/Z - 1j*U #the complex flow 
   
    
    return w, c_flow.imag, Airfoil

def update(val):
	r=srad.val
	ratio=sratio.val
	alpha = salpha.val
	const = sconst.val
	
	ax.clear()
	xy,psi,foil = flowlines(alpha=alpha,R=r,ratio=ratio,const=const)
	ax.plot(foil.real,foil.imag)
	ax.contour(xy.real,xy.imag,psi,levels,linestyles='solid')	
	
	fig.canvas.draw_idle()




levels = np.arange(-3, 3.7, 0.25)
xy,psi,foil = flowlines()

fig,ax=plt.subplots()
plt.subplots_adjust(left=0.25,bottom=0.25)
ax.plot(foil.real,foil.imag)
ax.contour(xy.real,xy.imag,psi,levels,linestyles='solid')

axr = plt.axes([0.25, 0.05, 0.65, 0.03])
axratio = plt.axes([0.25, 0.15, 0.65, 0.03])
axconst = plt.axes([0.25, 0.20, 0.65, 0.03])
axalpha = plt.axes([0.25,0.1,0.65,0.03])

srad = Slider(axr,'radius',0.5,2.0,valinit=1.)
sratio =  Slider(axratio,'ratio',0.5,2.0,valinit=1.2)
sconst =  Slider(axconst,'constant',-2.0,5.0,valinit=4.)
salpha = Slider(axalpha,'alpha',-45.,45.,valinit=0)

srad.on_changed(update)
sratio.on_changed(update)
sconst.on_changed(update)
salpha.on_changed(update)


plt.show()