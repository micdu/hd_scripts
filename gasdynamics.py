import numpy as np
import matplotlib.pyplot as plt

machnumber = np.arange(50)/49. * 2.5

gamma = 1.4
T_0 = 1.
rho_0 = 1.
temperature = T_0 * (1. - (gamma - 1.) / (gamma + 1.) * machnumber**2)
rho = rho_0 * (1. - (gamma - 1.) / (gamma + 1.)
                 * machnumber**2)**(1. / (gamma - 1.))
pressure = rho * temperature
rho_v = machnumber * rho

fig,axes = plt.subplots(2,2,sharex=True)

axes[0,0].plot(machnumber,temperature)
axes[0,0].set_ylabel('Temperature')
axes[0,1].plot(machnumber,pressure)
axes[0,1].set_ylabel('Pressure')
axes[1,0].plot(machnumber,rho)
axes[1,0].set_ylabel('Rho')
axes[1,0].set_xlabel('Mach number')
axes[1,1].plot(machnumber, rho_v)
axes[1,1].set_ylabel('rho*v')
axes[1,1].set_xlabel('Mach number')

plt.show()
