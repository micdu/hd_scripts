import numpy as np
import matplotlib.pyplot as plt

'''
Program to visualise the flow over an obstacle
      h(x): height of the flowing layer
      v(x): speed
      e(x): height of the obstacle - parameterised by height e0 at x0 with width delta
      h0,v0: starting conditions

      caution: the integration often has an issue when the Froude-number changes from <1 to >1, since the coefficient function of the differential equation becomes singular at Fr = 1
'''
g = 9.81 #ms^-2
e0 = 50.
x0 = 500.
delta = 100.

npts = 1000
x = np.arange(npts)

e_x = e0 * np.exp(-1 / (2. * delta**2) * (x - x0)**2)
de_dx = e_x * (-2. * (x - x0) / (2. * delta**2))



def solve(npts,h0,v0):
    h = np.zeros(npts)
    v = np.zeros(npts)
    froude = np.zeros(npts)
    h[0] = h0
    v[0] = v0

    for i in range(npts-1):
        dhdx = de_dx[i] / (v[i]**2 / (g * h[i]) - 1.)
        if np.isfinite(dhdx):
            h[i+1] = h[i] + dhdx
            v[i+1] = v[i] - v[i] / h[i+1] * dhdx
        else:
            h[i+1] = h[i]
            v[i+1] = v[i] 
        froude[i] = v[i] / np.sqrt(g * h[i])
    froude[-1] = froude[-2]

    return(h,v,froude)

fig,(ax1,ax2,ax3) = plt.subplots(3,1,sharex=True)
def plot(x,e_x,h,v,froude):
    ax1.plot(x,e_x,label = 'Object height')

    ax2.plot(x,h+e_x,label='Streamline height')
    ax2.plot(x,v,label='Velocity')
    ax3.plot(x,froude,label='Froude num.')
    plt.draw()

#gravity dominated - streamline gets lower + faster
'''
h0 = 130. # m
v0 = 10. #ms^-1
h,v,froude = solve(npts,h0,v0)
plot(x,e_x,h,v,froude)

#inertia dominated - streamline gets higher + slower
h0 = 130. # m
v0 = 100. #ms^-1
h,v,froude = solve(npts,h0,v0)
plot(x,e_x,h,v,froude)
'''
#Froude = 1 at top of the hill
h0 = 126.
v0 = 10.
h,v,froude = solve(npts,h0,v0)
plot(x,e_x,h,v,froude)


ax1.legend()
ax2.legend()
ax3.legend()
ax3.set_xlabel('Distance')

plt.show()
