import numpy as np
import matplotlib.pyplot as plt

rho1 = 1e3 #water
rho2 = 1.2 #air
alpha = 72e-3 #water
g = 9.81

k = (np.arange(100)+1)/0.1
fig,ax = plt.subplots(1)
ax.set_xscale('log')
ax.plot(k,np.sqrt(rho1 / rho2 * g / k),label = 'No surface tension')
ax.plot(k,np.sqrt(rho1 / rho2 * g / k + alpha * k / rho2),label = 'W. Surface tension')
ax.set_xlabel("Wavenumber k")
ax.set_ylabel("Delta u")
ax.legend()
plt.show()