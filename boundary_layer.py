import numpy as np
import matplotlib.pyplot as plt

nx = 100

def grids(nx):
	x = np.tile((np.arange(nx)/(nx-1.)*2.-1.) * 1.,(nx,1))
	y=np.transpose(x)

	p0 = 1.
	p = p0* x**2
	pprime = 2. * p0 * x
	ppprime = 2. * p0

	vx = 0.5 * pprime * y**2  + (np.sqrt(1. - 2.*p) - 0.5 * pprime) * y
	vy = -1./6. * ppprime * y**3 + 0.5 * ((2 * pprime) / (np.sqrt(1.-2*p))) * y**2
	vz = np.sqrt(vx**2 + vy**2) * np.sign(vx)
	
	return x,y,vx,vy,vz

fig,ax = plt.subplots()
x,y,vx,vy,vz = grids(300)
print(np.nanmin(vz),np.nanmax(vz))
plt.contourf(x,y,vz,1000,vmin = -1,vmax = 1)

x,y,vx,vy,vz = grids(30)
q = ax.quiver(x,y,vx,vy)
ax.set_ylim([0,1])
plt.show()