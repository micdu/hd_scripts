import numpy as np
import matplotlib.pyplot as plt

#z = np.arange(100)/99. - 0.5
utop = 1.
ubot = utop + np.sqrt(1.2)

rhotop = 0.91e3 #oil
rhobot = 1e3 #water
#rhobot = 1.2 #air

xi_hat = 0.01

k = 2.*np.pi / 3.33
g=9.81
x = np.arange(1000) / 99.

omega = (k * (ubot * rhobot + utop * rhotop) / (rhotop + rhobot) 
		+ np.lib.scimath.sqrt((k * (ubot *rhobot + utop * rhotop) / (rhotop+ rhobot))**2 
		+ g * k * (rhobot - rhotop) / (rhotop + rhobot) 
		- k**2 * (ubot**2 * rhobot + utop**2 * rhotop) / (rhotop + rhobot)))
print(omega)

time = np.arange(1000) / 99.
xi_xt = []
for t in time:
	if omega.imag != 0:
		xi_xt.append(xi_hat * np.exp(omega.imag * t) * np.cos(k * x - omega.real * t))
	else:
		xi_xt.append(xi_hat * np.cos(k * x - omega * t)	)



x,t = np.meshgrid(x,time)
xi_xt = np.asarray(xi_xt)
fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
surf = ax.plot_surface(x,t,xi_xt)
ax.set_xlabel('x')
ax.set_ylabel('Time')
ax.set_zlabel('Amplitude')


plt.show() 



