import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

nx = 100
r = np.tile((np.arange(nx)/(nx-1.) * 2. - 1.) * 2.,(nx,1))
theta = np.tile((np.arange(nx)/(nx-1.) * 2. - 1.) *np.pi,(nx,1)).transpose()

x = r * np.cos(theta)
y = r * np.sin(theta)

xreal = r * np.cos(theta)
yreal = r * np.sin(theta)

fig,ax=plt.subplots()
plt.subplots_adjust(left=0.25, bottom=0.25)
ax.contour(xreal,yreal,y,linestyles='solid')

axalpha=plt.axes([0.25,0.1,0.65,0.03])
salpha=Slider(axalpha,'Alpha',0.,5.,valinit=1.)

axphi=plt.axes([0.25,0.05,0.65,0.03])
sphi=Slider(axphi,'Phi',0.,np.pi,valinit=0.)

def update(val):
    alpha=salpha.val
    phi=sphi.val
    
    x = (r**alpha) * np.cos(alpha*theta + phi)
    y = (r**alpha) * np.sin(alpha*theta + phi) 
    
    ax.clear()
    ax.contour(xreal,yreal,y,linestyles='solid')
    fig.canvas.draw_idle()

salpha.on_changed(update)
sphi.on_changed(update)

plt.show()
