import numpy as np
import matplotlib.pyplot as plt

x = (np.arange(100)/99. - 0.5) * 10.
F = 0.1 * x**2 + 1.
Fmin = 1.

Ma_max = 1.
gamma = 1.4
T_0 = 1.
rho_0 = 1.

ma_0 = Ma_max * np.sqrt((gamma-1)/2.)

machnumber = np.arange(250)/249. * 2.5
temperature = T_0 * (1. - (gamma - 1.) / (gamma + 1.) * machnumber**2)
rho = rho_0 * (1. - (gamma - 1.) / (gamma + 1.)
                 * machnumber**2)**(1. / (gamma - 1.))
pressure = rho * temperature
rho_v = machnumber * rho

rhov_x = np.interp(Ma_max,machnumber,rho_v) / F

mach_nozzle = np.zeros(100)
subsonic = machnumber < 1.
supersonic = machnumber > 1
for i in range(100):
	if x[i] < 0:
		mach_nozzle[i] = np.interp(rhov_x[i],rho_v[subsonic],machnumber[subsonic])
	elif x[i] > 0:
		if Ma_max == 1:
			rv_temp = np.flip(rho_v[supersonic])
			ma_temp = np.flip(machnumber[supersonic])
		else:	
			rv_temp = rho_v[subsonic]
			ma_temp = machnumber[subsonic]
		mach_nozzle[i] = np.interp(rhov_x[i],rv_temp,ma_temp)
	else:
		mach_nozzle[i] = Ma_max
	

temperature = T_0 * (1. - (gamma - 1.) / (gamma + 1.) * mach_nozzle**2)
rho = rho_0 * (1. - (gamma - 1.) / (gamma + 1.)
                 * mach_nozzle**2)**(1. / (gamma - 1.))
pressure = rho * temperature


plt.plot(x,np.sqrt(F/np.pi),label = 'Nozzle radius' )
plt.plot(x,-np.sqrt(F/np.pi),color='C0')
plt.plot(x,rhov_x, label = 'rho*v')
plt.plot(x,mach_nozzle,label = 'Ma')
plt.plot(x, pressure, label = 'Pressure')
plt.legend()
plt.show()